package exception;

/**
 * Created by Босс on 02.02.2017.
 */
public class WrongElementException extends Exception {

    public WrongElementException(String message) {
        super(message);
    }
}
