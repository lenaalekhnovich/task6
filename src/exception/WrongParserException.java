package exception;

/**
 * Created by Босс on 06.02.2017.
 */
public class WrongParserException extends Exception{

    public WrongParserException(String message) {
        super(message);
    }
}
