package parsing;

import object.*;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Босс on 31.01.2017.
 */
public class DOMParserBuilder extends ParserBuilder {

    static Logger logger = Logger.getLogger(DOMParserBuilder.class);

    private DocumentBuilder docBuilder;

    public DOMParserBuilder() {
        super();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.error("negative argument:" + e);
        }
    }

    @Override
    public void parse(String fileName) {
        Document document;
        try {
            document = docBuilder.parse(fileName);
            Element root = document.getDocumentElement();
            NodeList voucherList = root.getElementsByTagName("restVoucher");
            for (int i = 0; i < voucherList.getLength(); i++) {
                Element voucherElement = (Element) voucherList.item(i);
                RestVoucher voucher = buildRestVoucher(voucherElement);
                vouchers.add(voucher);
            }
            voucherList = root.getElementsByTagName("excursionVoucher");
            for (int i = 0; i < voucherList.getLength(); i++) {
                Element voucherElement = (Element) voucherList.item(i);
                ExcursionVoucher voucher = buildExcursionVoucher(voucherElement);
                vouchers.add(voucher);
            }
            voucherList = root.getElementsByTagName("wellnessVoucher");
            for (int i = 0; i < voucherList.getLength(); i++) {
                Element voucherElement = (Element) voucherList.item(i);
                WellnessVoucher voucher = buildWellnessVoucher(voucherElement);
                vouchers.add(voucher);
            }
        } catch (SAXException e) {
            logger.error("negative argument: " + e);
        } catch (IOException e) {
            logger.error("negative argument: " + e);
        }
    }

    private RestVoucher buildRestVoucher(Element elementVoucher) {
        RestVoucher voucher = (RestVoucher) buildVoucher(new RestVoucher(), elementVoucher);
        Element elementHotel = (Element) elementVoucher.getElementsByTagName("hotel").item(0);
        Hotel hotel = buildHotel(elementHotel);
        voucher.setHotel(hotel);
        return voucher;
    }

    private ExcursionVoucher buildExcursionVoucher(Element elementVoucher) {
        List<String> excursions = new LinkedList<>();
        ExcursionVoucher voucher = (ExcursionVoucher) buildVoucher(new ExcursionVoucher(), elementVoucher);
        Element elementExcursions = (Element) elementVoucher.getElementsByTagName("excursions").item(0);
        NodeList excursionsList = elementExcursions.getElementsByTagName("excursion");
        for (int i = 0; i < excursionsList.getLength(); i++) {
            excursions.add(excursionsList.item(i).getTextContent());
        }
        voucher.setExcursions(excursions);
        return voucher;
    }

    private WellnessVoucher buildWellnessVoucher(Element elementVoucher) {
        WellnessVoucher voucher = (WellnessVoucher) buildVoucher(new WellnessVoucher(), elementVoucher);
        Element elementResort = (Element) elementVoucher.getElementsByTagName("resort").item(0);
        Resort resort = buildResort(elementResort);
        voucher.setResort(resort);
        return voucher;
    }

    private TouristVoucher buildVoucher(TouristVoucher voucher, Element elementVoucher) {
        voucher.setId(elementVoucher.getAttribute("id"));
        Element countryElement = (Element) elementVoucher.getElementsByTagName("country").item(0);
        if (countryElement.getAttribute("code") != null) {
            voucher.setCodeCountry(Integer.parseInt(countryElement.getAttribute("code")));
        }
        voucher.setCountry(countryElement.getTextContent());
        String amountDaysNights = getElementTextContent(elementVoucher, "amountDaysNights");
        int days = Integer.parseInt(amountDaysNights.substring(0, amountDaysNights.indexOf(" ")).trim());
        int nights = Integer.parseInt(amountDaysNights.substring(amountDaysNights.indexOf(" ")).trim());
        voucher.setAmountDays(days);
        voucher.setAmountNights(nights);
        Element transportElement = (Element) elementVoucher.getElementsByTagName("transport").item(0);
        if (transportElement.getAttribute("kind") != null) {
            voucher.setTransportName(transportElement.getAttribute("kind"));
        }
        voucher.setTransport(transportElement.getTextContent());
        voucher.setCost(Integer.parseInt(getElementTextContent(elementVoucher, "cost")));
        return voucher;
    }

    private Hotel buildHotel(Element elementHotel) {
        Hotel hotel = new Hotel();
        hotel.setName(getElementTextContent(elementHotel, "nameHotel"));
        hotel.setStars(Integer.parseInt(getElementTextContent(elementHotel, "stars")));
        Element foodElement = (Element) elementHotel.getElementsByTagName("food").item(0);
        if (foodElement.getAttribute("kind") != null) {
            hotel.setTypeFood(foodElement.getAttribute("kind"));
        }
        hotel.setFoodIncluded(Boolean.parseBoolean(foodElement.getTextContent()));
        hotel.setRoomType(getElementTextContent(elementHotel, "room"));
        hotel.setTvIncluded(Boolean.parseBoolean(getElementTextContent(elementHotel, "tv")));
        hotel.setConditioningIncluded(Boolean.parseBoolean(getElementTextContent(elementHotel, "airConditioning")));
        hotel.setEmail(getElementTextContent(elementHotel, "email"));
        return hotel;
    }

    private Resort buildResort(Element elementResort) {
        Resort resort = new Resort();
        resort.setName(getElementTextContent(elementResort, "nameResort"));
        Element elementProcedures = (Element) elementResort.getElementsByTagName("procedures").item(0);
        NodeList procedureList = elementProcedures.getElementsByTagName("procedure");
        for (int i = 0; i < procedureList.getLength(); i++) {
            resort.addProcedure(procedureList.item(i).getTextContent());
        }
        return resort;
    }

    private String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        String text = node.getTextContent();
        return text;
    }

}
