package parsing;

import exception.WrongElementException;
import object.*;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Босс on 31.01.2017.
 */
public class VoucherHandler extends DefaultHandler {

    static Logger logger = Logger.getLogger(VoucherHandler.class);

    private List<TouristVoucher> vouchers;
    private TouristVoucher currentVoucher = null;
    private VoucherEnum currentElement = null;

    private Hotel hotel = null;
    private Resort resort = null;
    private List<String> excursions = null;

    public VoucherHandler() {
        vouchers = new LinkedList<>();
    }

    public List<TouristVoucher> getVouchers() {
        return vouchers;
    }

    public void startElement(String uri, String localName, String qName, Attributes attrs) {
        switch (VoucherEnum.valueOf(localName.toUpperCase())) {
            case RESTVOUCHER:
                currentVoucher = new RestVoucher();
                currentVoucher.setId(attrs.getValue(0));
                break;
            case WELLNESSVOUCHER:
                currentVoucher = new WellnessVoucher();
                currentVoucher.setId(attrs.getValue(0));
                break;
            case EXCURSIONVOUCHER:
                currentVoucher = new ExcursionVoucher();
                currentVoucher.setId(attrs.getValue(0));
                break;
            case COUNTRY:
                currentVoucher.setCodeCountry(Integer.parseInt(attrs.getValue(0)));
                currentElement = VoucherEnum.valueOf(localName.toUpperCase());
            case TRANSPORT:
                if (attrs.getLength() == 1) {
                    currentVoucher.setTransportName(attrs.getValue(0));
                }
                currentElement = VoucherEnum.valueOf(localName.toUpperCase());
            case HOTEL:
                hotel = new Hotel();
                break;
            case FOOD:
                if (attrs.getLength() == 1) {
                    hotel.setTypeFood(attrs.getValue(0));
                }
                currentElement = VoucherEnum.valueOf(localName.toUpperCase());
                break;
            case RESORT:
                resort = new Resort();
                break;
            case EXCURSIONS:
                excursions = new LinkedList<>();
                break;
            default:
                currentElement = VoucherEnum.valueOf(localName.toUpperCase());
        }
    }

    public void endElement(String uri, String localName, String qName) {
        try {
            switch (VoucherEnum.valueOf(localName.toUpperCase())) {
                case RESTVOUCHER:
                    vouchers.add(currentVoucher);
                    break;
                case WELLNESSVOUCHER:
                    vouchers.add(currentVoucher);
                    break;
                case EXCURSIONVOUCHER:
                    vouchers.add(currentVoucher);
                    break;
                case HOTEL:
                    currentVoucher.setHotel(hotel);
                    break;
                case RESORT:
                    currentVoucher.setResort(resort);
                    break;
                case EXCURSIONS:
                    currentVoucher.setExcursions(excursions);
                    break;
            }
        } catch (WrongElementException e) {
            logger.error("negative argument: " + e);
        }
    }

    public void characters(char[] character, int start, int length) {
        String currentString = new String(character, start, length).trim();
        if (currentElement != null && !currentString.equals("")) {
            switch (currentElement) {
                case COUNTRY:
                    currentVoucher.setCountry(currentString);
                    break;
                case AMOUNTDAYSNIGHTS:
                    int days = Integer.parseInt(currentString.substring(0, currentString.indexOf(" ")).trim());
                    int nights = Integer.parseInt(currentString.substring(currentString.indexOf(" ")).trim());
                    currentVoucher.setAmountDays(days);
                    currentVoucher.setAmountNights(nights);
                    break;
                case TRANSPORT:
                    currentVoucher.setTransport(currentString);
                    break;
                case COST:
                    currentVoucher.setCost(Integer.parseInt(currentString));
                    break;
                case NAMEHOTEL:
                    hotel.setName(currentString);
                    break;
                case STARS:
                    hotel.setStars(Integer.parseInt(currentString));
                    break;
                case FOOD:
                    hotel.setFoodIncluded(Boolean.parseBoolean(currentString));
                    break;
                case ROOM:
                    hotel.setRoomType(currentString);
                    break;
                case TV:
                    hotel.setTvIncluded(Boolean.parseBoolean(currentString));
                    break;
                case AIRCONDITIONING:
                    hotel.setConditioningIncluded(Boolean.parseBoolean(currentString));
                    break;
                case EMAIL:
                    hotel.setEmail(currentString);
                    break;
                case NAMERESORT:
                    resort.setName(currentString);
                    break;
                case PROCEDURE:
                    resort.addProcedure(currentString);
                    break;
                case EXCURSION:
                    excursions.add(currentString);
                    break;
            }
        }
    }
}
