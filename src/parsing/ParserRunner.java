package parsing;

import exception.WrongParserException;
import object.TouristVoucher;

import java.util.List;

/**
 * Created by Босс on 02.02.2017.
 */
public class ParserRunner {

    public enum TypeParser{
        DOM, SAX, STAX
    }

    private ParserBuilder parserBuilder;

    public List<TouristVoucher> getVouchers() {
        return parserBuilder.getVouchers();
    }

    public void createTouristVouchers(String fileName) {
        parserBuilder.parse(fileName);
    }

    public void createParserBuilder(String parser) throws WrongParserException {
        switch(TypeParser.valueOf(parser.toUpperCase())){
            case DOM:
                parserBuilder = new DOMParserBuilder();
                break;
            case SAX:
                parserBuilder = new SAXParserBuilder();
                break;
            case STAX:
                parserBuilder = new StAXParserBuilder();
                break;
            default:
                throw new WrongParserException("Parser type doesn't exist");
        }
    }
}
