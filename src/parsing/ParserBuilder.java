package parsing;

import object.TouristVoucher;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Босс on 31.01.2017.
 */
public abstract class ParserBuilder {

    protected List<TouristVoucher> vouchers;

    public ParserBuilder() {
        vouchers = new LinkedList<>();
    }

    public List<TouristVoucher> getVouchers() {
        return vouchers;
    }

    abstract public void parse(String filename);
}
