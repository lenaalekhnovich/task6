package parsing;

import exception.WrongElementException;
import object.*;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Босс on 31.01.2017.
 */
public class StAXParserBuilder extends ParserBuilder {

    static Logger logger = Logger.getLogger(StAXParserBuilder.class);

    private XMLInputFactory inputFactory;

    public StAXParserBuilder() {
        inputFactory = XMLInputFactory.newInstance();
    }

    @Override
    public void parse(String fileName) {
        FileInputStream inputStream = null;
        XMLStreamReader reader = null;
        String name;
        try {
            inputStream = new FileInputStream(new File(fileName));
            reader = inputFactory.createXMLStreamReader(inputStream);
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = reader.getLocalName();
                    switch (VoucherEnum.valueOf(name.toUpperCase())) {
                        case RESTVOUCHER:
                            RestVoucher restVoucher = buildRestVoucher(reader);
                            vouchers.add(restVoucher);
                            break;
                        case WELLNESSVOUCHER:
                            WellnessVoucher wellnessVoucher = buildWellnessVoucher(reader);
                            vouchers.add(wellnessVoucher);
                            break;
                        case EXCURSIONVOUCHER:
                            ExcursionVoucher excursionVoucher = buildExcursionVoucher(reader);
                            vouchers.add(excursionVoucher);
                            break;
                    }
                }
            }
        } catch (XMLStreamException e) {
            logger.error("StAX parsing error! " + e);
        } catch (FileNotFoundException e) {
            logger.error("File " + fileName + " not found! " + e);
        } catch (WrongElementException e) {
            logger.error("negative argument: " + e);
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                logger.error("Impossible close file " + fileName + " : " + e);
            }
        }
    }

    public RestVoucher buildRestVoucher(XMLStreamReader reader) throws WrongElementException, XMLStreamException {
        RestVoucher voucher = (RestVoucher) buildTouristVoucher(reader, new RestVoucher());
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    if (name.equals("hotel")) {
                        Hotel hotel = buildHotel(reader);
                        voucher.setHotel(hotel);
                        break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (name.equals("restVoucher")) {
                        return voucher;
                    }
                    break;
            }
        }
        throw new WrongElementException("Couldn't find element restVoucher");
    }

    public ExcursionVoucher buildExcursionVoucher(XMLStreamReader reader) throws XMLStreamException, WrongElementException {
        ExcursionVoucher voucher = (ExcursionVoucher) buildTouristVoucher(reader, new ExcursionVoucher());
        List<String> excursions = new LinkedList<>();
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    if (name.equals("excursion")) {
                        name = getXMLText(reader);
                        excursions.add(name);
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (name.equals("excursionVoucher")) {
                        voucher.setExcursions(excursions);
                        return voucher;
                    }
                    break;
            }
        }
        throw new WrongElementException("Couldn't find element excursionVoucher");
    }

    public WellnessVoucher buildWellnessVoucher(XMLStreamReader reader) throws XMLStreamException, WrongElementException {
        WellnessVoucher voucher = (WellnessVoucher) buildTouristVoucher(reader, new WellnessVoucher());
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    if (name.equals("resort")) {
                        Resort resort = buildResort(reader);
                        voucher.setResort(resort);
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (name.equals("wellnessVoucher")) {
                        return voucher;
                    }
                    break;
            }
        }
        throw new WrongElementException("Couldn't find element wellnessVoucher");
    }

    public TouristVoucher buildTouristVoucher(XMLStreamReader reader, TouristVoucher voucher) throws XMLStreamException, WrongElementException {
        voucher.setId(reader.getAttributeValue(null, "id"));
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (VoucherEnum.valueOf(name.toUpperCase())) {
                        case COUNTRY:
                            voucher.setCodeCountry(Integer.parseInt(reader.getAttributeValue(null, "code")));
                            name = getXMLText(reader);
                            voucher.setCountry(name);
                            break;
                        case AMOUNTDAYSNIGHTS:
                            name = getXMLText(reader);
                            int days = Integer.parseInt(name.substring(0, name.indexOf(" ")).trim());
                            int nights = Integer.parseInt(name.substring(name.indexOf(" ")).trim());
                            voucher.setAmountDays(days);
                            voucher.setAmountNights(nights);
                            break;
                        case TRANSPORT:
                            voucher.setTransportName(reader.getAttributeValue(null, "kind"));
                            name = getXMLText(reader);
                            voucher.setTransport(name);
                            break;
                        case COST:
                            name = getXMLText(reader);
                            voucher.setCost(Integer.parseInt(name));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (name.equals("cost")) {
                        return voucher;
                    }
                    break;
            }
        }
        throw new WrongElementException("Couldn't find element cost");
    }

    public Hotel buildHotel(XMLStreamReader reader) throws XMLStreamException, WrongElementException {
        Hotel hotel = new Hotel();
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (VoucherEnum.valueOf(name.toUpperCase())) {
                        case NAMEHOTEL:
                            name = getXMLText(reader);
                            hotel.setName(name);
                            break;
                        case STARS:
                            name = getXMLText(reader);
                            hotel.setStars(Integer.parseInt(name));
                            break;
                        case FOOD:
                            hotel.setTypeFood(reader.getAttributeValue(null, "kind"));
                            name = getXMLText(reader);
                            hotel.setFoodIncluded(Boolean.parseBoolean(name));
                            break;
                        case ROOM:
                            name = getXMLText(reader);
                            hotel.setRoomType(name);
                            break;
                        case TV:
                            name = getXMLText(reader);
                            hotel.setTvIncluded(Boolean.parseBoolean(name));
                            break;
                        case AIRCONDITIONING:
                            name = getXMLText(reader);
                            hotel.setConditioningIncluded(Boolean.parseBoolean(name));
                            break;
                        case EMAIL:
                            name = getXMLText(reader);
                            hotel.setEmail(name);
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (name.equals("hotel")) {
                        return hotel;
                    }
            }
        }
        throw new WrongElementException("Couldn't find element hotel");
    }

    private Resort buildResort(XMLStreamReader reader) throws WrongElementException, XMLStreamException {
        Resort resort = new Resort();
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (VoucherEnum.valueOf(name.toUpperCase())) {
                        case NAMERESORT:
                            name = getXMLText(reader);
                            resort.setName(name);
                            break;
                        case PROCEDURE:
                            name = getXMLText(reader);
                            resort.addProcedure(name);
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (name.equals("resort")) {
                        return resort;
                    }
            }
        }
        throw new WrongElementException("Couldn't find element resort");
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }

}
