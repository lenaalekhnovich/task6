package test;

import exception.WrongParserException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import parsing.ParserRunner;
import validator.ValidatorXSD;

/**
 * Created by Босс on 31.01.2017.
 */
public class ParsingTest {

    private static ParserRunner parserRunner;
    private static String fileName = "src/resources/info.xml";
    private static String schemaName = "src/resources/schema.xsd";

    @BeforeClass
    public static void init() {
        parserRunner = new ParserRunner();
    }

    @Test
    public void validateDocumentTest() {
        boolean check = ValidatorXSD.validateDocument(fileName, schemaName);
        Assert.assertTrue(check);
    }

    @Test
    public void DOMParsingTest() throws WrongParserException {
        parserRunner.createParserBuilder("DOM");
        parserRunner.createTouristVouchers(fileName);
        Assert.assertNotNull(parserRunner.getVouchers());
    }

    @Test
    public void SAXParsingTest() throws WrongParserException {
        parserRunner.createParserBuilder("SAX");
        parserRunner.createTouristVouchers(fileName);
        Assert.assertNotNull(parserRunner.getVouchers());
    }

    @Test
    public void StAXParsingTest() throws WrongParserException {
        parserRunner.createParserBuilder("StAX");
        parserRunner.createTouristVouchers(fileName);
        Assert.assertNotNull(parserRunner.getVouchers());
    }
}
