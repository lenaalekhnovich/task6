package servlet;

import exception.WrongParserException;
import object.TouristVoucher;
import org.apache.log4j.Logger;
import parsing.ParserRunner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/parsing")
public class ApplicationServlet extends HttpServlet {

    private List<TouristVoucher> listVoucher;
    static Logger logger = Logger.getLogger(ApplicationServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (WrongParserException e) {
            logger.error("negative argument:" + e);
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request,response);
        } catch (WrongParserException e) {
           logger.error("negative argument:" + e);
        }

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, WrongParserException {
        String parser;
        int page;
        if((parser = request.getParameter("parserButton")) != null) {
            request.setAttribute("res", parser);
            ParserRunner parserRunner = new ParserRunner();
            parserRunner.createParserBuilder(parser);
            parserRunner.createTouristVouchers("task6/src/resources/info.xml");
            listVoucher = parserRunner.getVouchers();
        }
        page = request.getParameter("pageButton") == null ? 1 : Integer.parseInt(request.getParameter("pageButton"));
        request.setAttribute("page", page);
        request.setAttribute("size", listVoucher.size());
        request.setAttribute("lst", listVoucher);
        request.getRequestDispatcher("/result.jsp").forward(request, response);
    }

}