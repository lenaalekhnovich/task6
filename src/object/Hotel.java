package object;

/**
 * Created by Босс on 30.01.2017.
 */
public class Hotel {

    private String name;
    private int stars;
    private boolean isFoodIncluded;
    private String typeFood;
    private boolean isTvIncluded;
    private boolean isConditioningIncluded;
    private String roomType;
    private String email;

    public void setName(String name) {
        this.name = name;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public void setFoodIncluded(boolean foodIncluded) {
        isFoodIncluded = foodIncluded;
    }

    public void setTypeFood(String typeFood) {
        this.typeFood = typeFood;
    }

    public void setTvIncluded(boolean tvIncluded) {
        isTvIncluded = tvIncluded;
    }

    public void setConditioningIncluded(boolean conditioningIncluded) {
        isConditioningIncluded = conditioningIncluded;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public int getStars() {
        return stars;
    }

    public boolean getIsFoodIncluded() {
        return isFoodIncluded;
    }

    public String getTypeFood() {
        return typeFood;
    }

    public boolean getIsTvIncluded() {
        return isTvIncluded;
    }

    public boolean getIsConditioningIncluded() {
        return isConditioningIncluded;
    }

    public String getRoomType() {
        return roomType;
    }

    public String getEmail() {
        return email;
    }
}
