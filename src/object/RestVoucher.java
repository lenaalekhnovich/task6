package object;

/**
 * Created by Босс on 31.01.2017.
 */
public class RestVoucher extends TouristVoucher {
    private Hotel hotel;

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    @Override
    public Hotel getHotel() {
        return hotel;
    }
}
