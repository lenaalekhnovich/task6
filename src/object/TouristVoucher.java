package object;

import exception.WrongElementException;

import java.util.List;

/**
 * Created by Босс on 30.01.2017.
 */
public abstract class TouristVoucher {

    private String id;
    private String country;
    private int codeCountry;
    private int amountDays;
    private int amountNights;
    private String transport;
    private String transportName;
    private int cost;

    public void setId(String id) {
        this.id = id;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCodeCountry(int codeCountry) {
        this.codeCountry = codeCountry;
    }

    public void setAmountDays(int amountDays) {
        this.amountDays = amountDays;
    }

    public void setAmountNights(int amountNights) {
        this.amountNights = amountNights;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public void setTransportName(String transportName) {
        this.transportName = transportName;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setHotel(Hotel hotel) throws WrongElementException {
        throw new WrongElementException("Element hotel doesn't exist in such voucher");
    }

    public void setResort(Resort resort) throws WrongElementException {
        throw new WrongElementException("Element resort doesn't exist in such voucher");
    }

    public void setExcursions(List<String> list) throws WrongElementException {
        throw new WrongElementException("Element excursions doesn't exist in such voucher");

    }

    public String getId() {
        return id;
    }

    public String getCountry() {
        return country;
    }

    public int getCodeCountry() {
        return codeCountry;
    }

    public int getAmountDays() {
        return amountDays;
    }

    public int getAmountNights() {
        return amountNights;
    }

    public String getTransport() {
        return transport;
    }

    public String getTransportName() {
        return transportName;
    }

    public int getCost() {
        return cost;
    }

    public Hotel getHotel(){
        return null;
    }

    public Resort getResort(){
        return null;
    }

    public List<String> getExcursions(){
        return null;
    }
}
