package object;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Босс on 31.01.2017.
 */
public class Resort {

    private String name;
    private List<String> procedures;

    public Resort() {
        procedures = new LinkedList<>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addProcedure(String procedure) {
        procedures.add(procedure);
    }

    public String getName() {
        return name;
    }

    public List<String> getProcedures() {
        return procedures;
    }


}
