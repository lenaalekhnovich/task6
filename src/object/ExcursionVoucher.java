package object;

import java.util.List;

/**
 * Created by Босс on 31.01.2017.
 */
public class ExcursionVoucher extends TouristVoucher {

    private List<String> excursions;

    public void setExcursions(List<String> excursions) {
        this.excursions = excursions;
    }

    public List<String> getExcursions() {
        return excursions;
    }
}
