package object;

/**
 * Created by Босс on 31.01.2017.
 */
public class WellnessVoucher extends TouristVoucher {

    private Resort resort;

    public void setResort(Resort resort) {
        this.resort = resort;
    }

    public Resort getResort() {
        return resort;
    }
}
