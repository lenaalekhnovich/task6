<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <title>Task6</title>
    <h4>Выберите парсер для разбора xml-документа</h4>
</head>

<body align = "center">
    <form name="Parser" action = "parsing" method="POST">
        <input type="submit" name="parserButton" value="DOM"/>
        <input type="submit" name="parserButton" value="SAX"/>
        <input type="submit" name="parserButton" value="StAX"/>
    </form>
</body>
</html>