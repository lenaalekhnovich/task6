<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Result Page</title>
    <h4>Туристически путевки</h4>
</head>

<body align = "center">
<table align = "center" border = "1" cellpadding="2">
    <tr align = "center">
        <td>id</td>
        <td>Страна</td>
        <td>Дней/ночей</td>
        <td>Транспорт</td>
        <td>Стоимость</td>
        <td>Тип путевки</td>
        <td>Дополнительная информация</td>
    </tr>

    <c:forEach var = "voucher" items = "${lst}" begin ="${(page-1)*4}" end = "${page*4-1}">
    <tr align = "center">
         <td>${voucher.id}</td>
         <td>${voucher.country}</td>
         <td>${voucher.amountDays}/${voucher.amountNights}</td>
         <td>${voucher.transport} ${voucher.transportName}</td>
         <td>${voucher.cost} $</td>
         <c:choose>
            <c:when test = "${not empty voucher.hotel}">
                <td>Туристическая</td>
                <td>
                    <p >Название отеля: ${voucher.hotel.name} ${voucher.hotel.stars} звезд(ы)
                    <br>Завтраки:
                        <c:choose>
                            <c:when test = "${voucher.hotel.isFoodIncluded}">Да (${voucher.hotel.typeFood})</c:when>
                            <c:otherwise>Нет </c:otherwise>
                        </c:choose>
                    <br>Телевизор:
                        <c:choose>
                            <c:when test = "${voucher.hotel.isConditioningIncluded}">Да</c:when>
                            <c:otherwise>Нет </c:otherwise>
                        </c:choose>
                    <br>Кондиционер:
                        <c:choose>
                            <c:when test = "${voucher.hotel.isConditioningIncluded}">Да</c:when>
                            <c:otherwise>Нет </c:otherwise>
                        </c:choose>
                    <br>Номер: ${voucher.hotel.roomType}
                    <br>Email: ${voucher.hotel.email}
                </td>
            </c:when>
            <c:when test = "${not empty voucher.resort}">
                <td>Оздоровительная</td>
                <td>Название санатория: ${voucher.resort.name}
                        <ul>
                            <c:forEach var = "procedure" items = "${voucher.resort.procedures}" >
                                <li> <c:out value = "${procedure}"/> </li>
                            </c:forEach>
                        </ul>
                </td>
            </c:when>
            <c:when test = "${not empty voucher.excursions}">
                <td>Экскурсионная</td>
                <td>
                    <ul>
                        <c:forEach var = "excursion" items = "${voucher.excursions}">
                            <li> <c:out value = "${excursion}"/> </li>
                        </c:forEach>
                    </ul>
                </td>
            </c:when>
         </c:choose>
    </tr>
    </c:forEach>
</table>

<p>
<form action = "parsing" method="POST">
    <c:forEach begin = "1" end = "${size / 4}" varStatus = "step">
        <input type="submit" name="pageButton" value="${step.count}"/>
    </c:forEach>
</form>
<a href = "/task6/index.jsp"> Вернуться к выбору парсера..</a>
</body>
</html>